//
//  ViewController.h
//  AppleMaps
//
//  Created by Click Labs134 on 10/13/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet MKMapView *mapview;

@end

